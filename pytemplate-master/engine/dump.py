import os
# import jinja2


TEMPLATES = "templates"


def generate_terraform_template(obj):
    resource = obj.resource
    path = os.path.join(TEMPLATES, resource)  # pylint: disable=unused-variable  # noqa
    # jinja2.dump(path, obj)
