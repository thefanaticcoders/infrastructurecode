from engine.dump import generate_terraform_template


class Base:
    resource = None

    def generate_template(self, backend='terraform'):
        if backend == 'terraform':
            return generate_terraform_template(self)
        raise NotImplementedError


class Server(Base):
    pass


class Network(Base):
    pass


class Subnet(Base):
    pass


class Router(Base):
    pass


class SecurityGroup(Base):
    pass


class KeyPair(Base):
    pass


class ElasticIP(Base):
    pass
