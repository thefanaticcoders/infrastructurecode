from engine.base import (Server,
                         SecurityGroup,
                         Network,
                         Subnet,
                         Router,
                         ElasticIP)


class Aws:
    '''Provider configuration
    '''


class AwsServer(Server):
    '''AWS server configuration
    '''
    resource = 'aws_server'

    def __init__(self):
        pass


class AwsSecurityGroup(SecurityGroup):
    '''AWS security group configuration
    '''


class AwsNetwork(Network):
    '''AWS network configuration
    '''


class AwsSubnet(Subnet):
    '''AWS subnet configuration
    '''


class AwsRouter(Router):
    '''AWS router configuration
    '''


class AwsElasticIP(ElasticIP):
    '''AWS elastic IP configuration
    '''
